module Main where

import Control.Monad           (msum)
import Data.Text               (pack, empty)
import Happstack.Server
import Web.Routes              ( PathInfo(..), RouteT, showURL
                               , runRouteT, Site(..), setDefault, mkSitePI)
import Web.Routes.TH           (derivePathInfo)
import Web.Routes.Happstack    (implSite)

import Common
import UserLevel
import AdminLevel

route :: Sitemap -> RouteT Sitemap (ServerPartT IO) String
route url = case url of
	-- Operations that do not require identification
	Notfound -> notFound ""
	(Login user passSalt) -> login user passSalt
	(Register user passSalt) -> reg user passSalt

	-- Admin operations that were protected by frontend server
	Adminlisttwiz -> AdminLevel.listTwiz
	Admincreatetwiz -> createTwiz
	(Adminviewtwiz tid) -> AdminLevel.viewTwiz tid
	(Adminedittwiz tid) -> editTwiz tid
	(Admindeletetwiz tid) -> deleteTwiz tid

	(Adminassignreview tid) -> assignReview tid
	(Admingetassignbuddy tid) -> getAssignBuddy tid
	(Adminreporttwiz tid) -> reportTwiz tid
	Adminsavereport -> saveReport

	Adminlistuser -> listUser
	(Adminswitchuser user) -> switchUser user

	_ -> do
		user <- lookCookieValue "user"
		-- TODO: Check user identification when necessary
		case url of
			Listtwiz -> UserLevel.listTwiz user
			(Viewtwiz tid) -> UserLevel.viewTwiz user tid
			(Viewreview tid) -> viewReview user tid
			(Posttwiz tid) -> postTwiz user tid
			(Postreview tid foruser) -> postReview user tid foruser
			(Viewfb tid) -> viewFeedback user tid

site :: Site Sitemap (ServerPartT IO String)
site = setDefault Notfound $ mkSitePI (runRouteT route)

main :: IO()
main = do
	putStrLn $ "Starting server"
	simpleHTTP nullConf {port=6000} $
		msum
			[ dir "favicon.ico" $ notFound ""
			, implSite (pack "http://10.0.1.2:6000") empty site
			]

