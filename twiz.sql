-- phpMyAdmin SQL Dump
-- version 4.0.1
-- http://www.phpmyadmin.net

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `twiz`
--

-- --------------------------------------------------------

--
-- Table `review`
--

CREATE TABLE IF NOT EXISTS `review` (
  `twizid` int(10) unsigned NOT NULL,
  `ruser` varchar(32) NOT NULL,
  `rdate` int(11) NOT NULL,
  `foruser` varchar(32) NOT NULL,
  `rcontent` text NOT NULL,
  PRIMARY KEY (`twizid`,`ruser`,`foruser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table `solution`
--

CREATE TABLE IF NOT EXISTS `solution` (
  `twizid` int(11) NOT NULL,
  `suser` varchar(32) NOT NULL,
  `sdate` int(11) NOT NULL,
  `scontent` text NOT NULL,
  PRIMARY KEY (`twizid`,`suser`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table `twiz`
--

CREATE TABLE IF NOT EXISTS `twiz` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) NOT NULL,
  `content` text NOT NULL,
  `due` int(11) NOT NULL,
  `rdue` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `name` varchar(32) NOT NULL,
  `salt` char(32) NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
