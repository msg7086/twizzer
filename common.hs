{-# LANGUAGE DeriveDataTypeable, TemplateHaskell #-}

module Common where

import Data.Data               (Data, Typeable)
import Web.Routes.TH           (derivePathInfo)
import Database.HDBC.MySQL
import Data.String.Utils       (replace)
--import Data.Convertible.Base

data Sitemap
    = Listtwiz
    | Viewtwiz Int | Viewreview Int
    | Posttwiz Int | Postreview Int String
    | Viewfb Int
    | Login String String
    | Register String String

    | Adminlisttwiz
    | Admincreatetwiz | Adminviewtwiz Int
    | Adminedittwiz Int | Admindeletetwiz Int
    
    | Admingetassignbuddy Int | Adminassignreview Int
    | Adminreporttwiz Int | Adminsavereport
    | Adminlistuser
    | Adminswitchuser String

    | Notfound
    deriving (Eq, Ord, Read, Show, Data, Typeable)

$(derivePathInfo ''Sitemap)

db :: IO Connection
db = connectMySQL defaultMySQLConnectInfo {
                        mysqlHost     = "127.0.0.1",
                        mysqlDatabase = "twiz",
                        mysqlUser     = "twiz",
                        mysqlPassword = "twiz"
                     }

parseString :: Maybe String -> String
parseString (Just s) = "\"" ++
           ( replace "\"" "\\\""
           $ replace "\n" "\\n"
           $ replace "\r\n" "\\n"
           $ replace "\\" "\\\\"
           $ s) ++ "\""
parseString Nothing = "null"

parseInt :: Maybe Int -> String
parseInt (Just x) = show x
parseInt Nothing = "0"

