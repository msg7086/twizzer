Date.prototype.format = function(format){
	var o = {
	"M+" : this.getMonth()+1, //month
	"d+" : this.getDate(),    //day
	"h+" : this.getHours(),   //hour
	"m+" : this.getMinutes(), //minute
	"s+" : this.getSeconds(), //second
	"q+" : Math.floor((this.getMonth()+3)/3),  //quarter
	"S" : this.getMilliseconds() //millisecond
	}
	if(this.getTime() == 0)
		return 'Not set';
	if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
	(this.getFullYear()+"").substr(4 - RegExp.$1.length));
	for(var k in o)if(new RegExp("("+ k +")").test(format))
	format = format.replace(RegExp.$1,
	RegExp.$1.length==1 ? o[k] :
	("00"+ o[k]).substr((""+ o[k]).length));
	return format;
}

;function listTwiz()
{
	$.getJSON('/adminlisttwiz', function (d) {
		var t = $('#admin-twiz-list').empty();
		for (var i = 0; i < d.length; i++) {
			twiz = d[i];
			var box = $('#twiz-box-source').clone().attr('id', '').removeClass('hide');
			$('.thumbnail', box).attr('twizid',twiz.id);

			$('h3', box).text(twiz.name);

			var today = new Date();
			var due = new Date();
			due.setTime(twiz.due * 1000);
			var rdue = new Date();
			rdue.setTime(twiz.rdue * 1000);
			$('.duedate', box).attr('class', due < today ? 'duedate text-error' : 'duedate').text(due.format('yyyy-MM-dd hh:mm'));
			$('.rduedate', box).attr('class', rdue < today ? 'text-error rduedate' : 'rduedate').text(rdue.format('yyyy-MM-dd hh:mm'));
			t.append(box);
		};
		t.append('<li><div class"thumbnail span1"><a href="#" class="btn btn-success btn-large" id="create-twiz"><i class="icon-plus icon-white"></i> New</a></div></li>');

		$('#create-twiz').click(createTwiz);

		$('#admin-twiz-list a.preview-twiz').click(editTwiz);
		$('#admin-twiz-list a.edit-twiz').click(editTwiz);
		$('#admin-twiz-list a.buddy-twiz').click(editTwiz);
		$('#admin-twiz-list a.del-twiz').click(deleteTwiz);
		$('#admin-twiz-list a.export-twiz').click(exportTwiz);
		$('#twiz-view-area').hide();
		$('#twiz-edit-area').hide();
	});
	return false;
}

;function createTwiz() {
	$.getJSON('/admincreatetwiz', function (d) {
		if(d.error == 0) {
			$.gritter.add({
				title: 'Success',
				class_name: 'alert-success',
				text: 'A new twiz created.<br />Proceed by clicking edit.'
			});
			listTwiz();
		} else {
			$.gritter.add({
				title: 'Error',
				class_name: 'alert-error',
				sticky: true,
				text: d.reason
			});
		}
	})
	return false;
}

;function editTwiz() {
	var me = $(this);
	var parent = me.parents('div.thumbnail');
	var button = 0;
	if(me.hasClass('edit-twiz')) button = 1;
	if(me.hasClass('buddy-twiz')) button = 2;
	var layout = 0;
	if($('#twiz-edit-area').is(':visible')) layout = 1;
	if($('#twiz-buddy-area').is(':visible')) layout = 2;

	if(parent.hasClass('active') && (button == layout))
		return false;
	$('#admin-twiz-list .active').removeClass('active');
	parent.addClass('active');
	twizid = parent.attr('twizid');
	if(!twizid)
		return;
	return button == 2 ? viewBuddy(twizid) : viewTwiz(twizid, button);
}
;function viewTwiz(twizid, isedit) {
	$.getJSON('/adminviewtwiz/'+twizid, function (problem) {
		if(isedit) {
			var t = $('#twiz-edit-area');
			var today = new Date();
			var due = new Date();
			if(problem.due)
				due.setTime(problem.due * 1000);
			else {
				due.setHours(12);
				due.setMinutes(0);
				due.setSeconds(0);
				due.setMilliseconds(0);
			}
			var rdue = new Date();
			if(problem.due)
				rdue.setTime(problem.rdue * 1000);
			else {
				rdue.setHours(12);
				rdue.setMinutes(0);
				rdue.setSeconds(0);
				rdue.setMilliseconds(0);
			}
			$('#due-date', t).val(due.format('yyyy-MM-dd hh:mm'));
			$('#rdue-date', t).val(rdue.format('yyyy-MM-dd hh:mm'));
			$('#due-date').datetimepicker('update');
			$('#rdue-date').datetimepicker('update');
			$('input#twizname', t).val(problem.name);
			$('textarea', t).val(problem.content);

			//viewReview(twizid);
			//viewFeedback(twizid);

			$('#twiz-view-area').hide();
			$('#twiz-buddy-area').hide();
			$('#twiz-edit-area').fadeIn();
			$.smoothScroll({scrollTarget: '#twiz-edit-area', offset: -60});
		} else {
			var t = $('#twiz-view-area');
			var converter = new Attacklab.showdown.converter();
			var twizcontent = converter.makeHtml(problem.content);
			var today = new Date();
			var due = new Date();
			due.setTime(problem.due * 1000);
			var rdue = new Date();
			rdue.setTime(problem.rdue * 1000);
			$('.duedate', t).attr('class', due < today ? 'duedate text-error' : 'duedate').text(due.format('yyyy-MM-dd hh:mm'));
			$('.rduedate', t).attr('class', rdue < today ? 'text-error rduedate' : 'rduedate').text(rdue.format('yyyy-MM-dd hh:mm'));
			t = $('#twiz-problem').empty();
			t.append($('<h1/>',{class:'clearfix'}).text(problem.name));
			t.append(twizcontent);

			sh_highlightDocument();
			$('#twiz-edit-area').hide();
			$('#twiz-buddy-area').hide();
			$('#twiz-view-area').fadeIn();
			$.smoothScroll({scrollTarget: '#twiz-view-area', offset: -60});
		}
	});
	return false;
}

;function viewBuddy(twizid) {
	$.getJSON('/admingetassignbuddy/'+twizid, function (d) {
		var t = $('#twiz-buddy-area #buddy-source').empty();
		var tickcount = 0;
		for (var i = 0; i < d.length; i++) {
			user = d[i];
			var label = $('<label/>', {class:'checkbox'});
			var checkbox = $('<input type="checkbox"/>');
			if(user.foruser == null && user.enabled) {
				checkbox.attr('checked', 'checked');
				tickcount++;
			}
			label.append(checkbox);
			label.append($('<i/>', {class:user.enabled?'icon-nothing':'icon-ban-circle'}));
			label.append($('<i/>', {class:user.scount?'icon-check icon-green':'icon-nothing'}));
			label.append($('<span/>', {class:'ruser text-info'}).text(user.name));
			if(user.foruser != null)
				label.append($('<span/>', {class:'foruser'}).text(': ' + user.foruser + ' '))
			while(user.rcount--)
				label.append($('<i/>', {class:'icon-edit icon-green'}));

			t.append(label);
		}

		// Initialize shuffle factor
		if(tickcount > 1)
		{
			var inputboxes = $('#buddy-shuffle input');
			for (var i = 0; i < inputboxes.length; i++) {
				$(inputboxes[i]).val(twizid * (i + 1) % (tickcount - 1) + 1);
			};
		}

		$('#twiz-edit-area').hide();
		$('#twiz-view-area').hide();
		$('#twiz-buddy-area').fadeIn();
		$.smoothScroll({scrollTarget: '#twiz-buddy-area', offset: -60});
	});
}

;function shuffleAssignBuddy() {
	var users = $('#buddy-source input:checked')
		.nextAll('span.ruser')
		.map(function (i,d) {return $(d).text();});

	var factors = $('#buddy-shuffle input')
		.map(function (i,d) {return parseInt($(d).val());})
		.filter(function (i,d) {return !isNaN(d) && d > 0});

	var text = '';
	for (var i = 0; i < users.length; i++) {
		text += users[i] + ':' +
			factors
				.map(function (ii,d) {return users[(i+d)%users.length];})
				.toArray()
				.join(',') + "\n";
	};

	var t = $('#buddy-submit textarea').val(text);
	return false;
}

;function saveAssignBuddy() {
	var form = $(this).parents('form');
	var postdata = $('textarea', form).val()
		.replace(/\n/g, ';')
		.replace(/\s/g, '');
	if(postdata == '')
	{
		viewBuddy(twizid);
		return false;
	}
	var twizid = $('#admin-twiz-list .active').attr('twizid');
	$.post('/adminassignreview/'+twizid, {buddies:postdata}, function (d) {
		viewBuddy(twizid);
	});
	return false;
}

;function exportTwiz() {
	var me = $(this);
	var parent = me.parents('div.thumbnail');
	twizid = parent.attr('twizid');
	if(!twizid)
		return;
	t = $('.export').empty();
	$.getJSON('/adminreporttwiz/'+twizid, function (d) {
		var converter = new Attacklab.showdown.converter();

		solutions = d.solution;
		reviews = d.review;
		for (var i = 0; i < solutions.length; i++) {
			var s = solutions[i];
			var sdate = new Date();
			sdate.setTime(s.sdate * 1000);
			var div = $('<div/>', {class:"row-fluid"});
			div.append($('<div/>', {class:"span2"}).html(s.suser + '<br/>' + sdate.format('yyyy-MM-dd hh:mm')));
			div.append($('<div/>', {class:"span10"}).html($('<pre class="sh_sourceCode sh_haskell">').text(s.scontent)));
			t.append(div);
		};
		for (var i = 0; i < reviews.length; i++) {
			var r = reviews[i];
			var rdate = new Date();
			rdate.setTime(r.rdate * 1000);
			var div = $('<div/>', {class:"row-fluid"});
			div.append($('<div/>', {class:"span2"}).html(r.ruser + ": " + r.foruser + '<br/>' + rdate.format('yyyy-MM-dd hh:mm')));
			div.append($('<div/>', {class:"span10"}).html(converter.makeHtml(r.rcontent)));
			t.append(div);
		};
		sh_highlightDocument();
		$('.save-report').show();
		$('.nav-tabs a:last').tab('show');
	});
}

;function saveReport() {
	var postdata = $('.export').html();
	$('.save-form textarea').val(postdata);
	$('.save-form').submit();
	return false;
}

;function saveTwiz() {
	// Update Unix Epoch time to hidden input box
	$('#due-date-epoch').val(Date.parse($('#due-date').val()) / 1000);
	$('#rdue-date-epoch').val(Date.parse($('#rdue-date').val()) / 1000);
	var form = $(this).parents('form');
	var postdata = form.serializeArray();
	var twizid = $('#admin-twiz-list .active').attr('twizid');
	$.post('/adminedittwiz/'+twizid, postdata, function (d) {
		if(d.error == 0) {
			$.gritter.add({
				title: 'Success',
				class_name: 'alert-success',
				text: d.length + ' bytes written.<br />' + d.record + ' records saved.'
			});
			listTwiz();
		} else {
			$.gritter.add({
				title: 'Error',
				class_name: 'alert-error',
				sticky: true,
				text: d.reason
			});
		}
	}, 'json');
	return false;
}

;function deleteTwiz() {
	var me = $(this);
	if(!me.hasClass('btn-mini'))
	{
		me.addClass('btn-mini')
			.removeClass('pull-right')
			.text('Confirm to delete');
		return false;
	}
	var parent = me.parents('div.thumbnail');
	twizid = parent.attr('twizid');
	if(!twizid)
		return false;
	$.getJSON('/admindeletetwiz/'+twizid, function (d) {
		if(d.error == 0) {
			$.gritter.add({
				title: 'Success',
				class_name: 'alert-success',
				text: 'The twiz is destroyed.'
			});
			listTwiz();
		} else {
			$.gritter.add({
				title: 'Error',
				class_name: 'alert-error',
				sticky: true,
				text: d.reason
			});
		}

	})
	return false;
}

;function listUser() {
	$.getJSON('/adminlistuser', function (d) {
		var t = $('#admin-user-list').empty();
		for (var i = 0; i < d.length; i++) {
			user = d[i];
			var box = $('#user-box-source').clone().attr('id', '').removeClass('hide');
			$('.thumbnail', box).attr('userid', user.id);
			if(!user.enabled)
				box.addClass('muted');
			$('.thumbnail', box).append(
				$('<a href="#" class="btn pull-right enable-user"></a>')
					.text(user.enabled ? 'Disable' : 'Enable')
					.click(enableUser)
			);

			$('h3', box).text(user.name);
			var icons;
			icons = $('.admin-icons-twiz', box);
			icons.append('<i class="icon-check icon-green"></i>' + user.scount);

			icons = $('.admin-icons-review', box);
			icons.append('<i class="icon-edit icon-green"></i>' + user.rcount);

			t.append(box);
		};
		$('#admin-user-list a.enable-user');
	});
	return false;
}

;function enableUser()
{
	var box = $(this).parents('li');
	var user = $('h3', box).text();
	$.get('/adminswitchuser/' + user, function () {
		listUser();
	});
}


;function bindButtonEvent()
{
	$('#due-date').datetimepicker({minView: 2, maxView: 3, autoclose: true});
	$('#rdue-date').datetimepicker({minView: 2, maxView: 3, autoclose: true});
	$('#buddy-shuffle a').click(shuffleAssignBuddy);
	$('.buddy-save').click(saveAssignBuddy);
	$('.save-report').click(saveReport);
	$('.twiz-save').click(saveTwiz);
}

;$(function () {
	listTwiz();
	listUser();

	bindButtonEvent();

});
