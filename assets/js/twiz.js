Date.prototype.format = function(format){
	var o = {
	"M+" : this.getMonth()+1, //month
	"d+" : this.getDate(),    //day
	"h+" : this.getHours(),   //hour
	"m+" : this.getMinutes(), //minute
	"s+" : this.getSeconds(), //second
	"q+" : Math.floor((this.getMonth()+3)/3),  //quarter
	"S" : this.getMilliseconds() //millisecond
	}
	if(/(y+)/.test(format)) format=format.replace(RegExp.$1,
	(this.getFullYear()+"").substr(4 - RegExp.$1.length));
	for(var k in o)if(new RegExp("("+ k +")").test(format))
	format = format.replace(RegExp.$1,
	RegExp.$1.length==1 ? o[k] :
	("00"+ o[k]).substr((""+ o[k]).length));
	return format;
}

;function listTwiz()
{
	$.getJSON('/listtwiz', function (d) {
		var t = $('#twiz-list').empty();
		for (var i = 0; i < d.length; i++) {
			twiz = d[i];
			var a = $('<a/>', {id:'show-twiz-'+twiz.id, href:'#twiz-'+twiz.id, twizid:twiz.id}).text(twiz.name);
			a.prepend('<i class="icon-chevron-right icon-right"></i>');
			var icons = $('<div class="icons-review"></div>').appendTo(a);
			while(twiz.rcount--)
				icons.append('<i class="icon-edit icon-green"></i>');
			if(twiz.scount)
			{
				icons = $('<div class="icons-twiz"></div>').appendTo(a);
				icons.append('<i class="icon-check icon-green"></i>');
			}
			t.append($('<li/>').append(a));
		};

		// Main user interface is ready to show
		$('#loading-ui').hide();
		$('#main-ui').show();
		
		// Bind event
		$('#twiz-list a').click(loadTwiz);
		$('#twiz-list a:first').click();
	});
	return false;
}

;function loadTwiz() {
	var me = $(this);
	if(me.parent().hasClass('active'))
		return false;
	$('#twiz-list .active').removeClass('active');
	me.parent().addClass('active');
	twizid = me.attr('twizid');
	if(!twizid)
		return;
	return viewTwiz(twizid);;
}

;function viewTwiz(twizid) {
	$.getJSON('/viewtwiz/'+twizid, function (d) {
		var converter = new Attacklab.showdown.converter();
		var problem = d.problem;
		var solution = d.solution;

		// Fill in content of the twiz problem
		var twizcontent = converter.makeHtml(problem.content);
		var t = $('#twiz-problem').empty();
		var today = new Date();
		var due = new Date();
		due.setTime(problem.due * 1000);
		var rdue = new Date();
		rdue.setTime(problem.rdue * 1000);
		$('.duedate').attr('class', due < today ? 'duedate text-error' : 'duedate').text(due.format('yyyy-MM-dd hh:mm'));
		$('.rduedate').attr('class', rdue < today ? 'text-error rduedate' : 'rduedate').text(rdue.format('yyyy-MM-dd hh:mm'));
		t.append($('<h1/>',{class:'clearfix'}).text(problem.name));
		t.append(twizcontent);

		// Fill in user's solution and check the due date
		if(solution == null) {
			$('#mysolution').text('-- No solution submitted so far.');
			$('#mysolution-duedate').attr('class', 'alert');
			$('#mysolution-duedate .submitdate').text('Not yet');
		} else {
			$('#mysolution').text(solution.content);
			$('#mysolution-duedate').attr('class', 'alert alert-info');
			var sdate = new Date();
			sdate.setTime(solution.date * 1000);
			$('#mysolution-duedate .submitdate').text(sdate.format('yyyy-MM-dd hh:mm'));
			$('#mysolution-textarea').val(solution.content);
		}

		viewReview(twizid);
		viewFeedback(twizid);

		$('.form-submit').hide();
	});
	return false;
}

;function viewReview(twizid) {
	$.getJSON('/viewreview/'+twizid, function (d) {
		//[{"date":0,"foruser":"xushua","rcontent":""},
		// {"date":0,"foruser":"liug","rcontent":""}]
		if(!$.isArray(d)) {
			$.gritter.add({
				title: 'Error',
				class_name: 'alert-error',
				text: 'Error occurred on loading reviews.'
			});
			return;
		}
		var converter = new Attacklab.showdown.converter();

		// Fill in reviews to work with
		var r = $('#myreview-list').empty();
		for (var i = 0; i < d.length; i++) {
			var review = d[i];
			var box = $('#review-box-source').clone().attr('id', '').removeClass('hide');
			$('span.ruser', box).text(review.foruser);
			$('pre.scontent', box).text(review.scontent ? review.scontent : '-- No solution submitted so far.');
			$('div.rcontent', box).html(converter.makeHtml(review.rcontent ? review.rcontent : '**No review submitted so far.**'));
			var rdate = new Date();
			rdate.setTime(review.date * 1000);
			$('.rsubmitdate', box)
				.text(review.date == 0 ? 'Not yet' : rdate.format('yyyy-MM-dd hh:mm'))
				.parent()
					.attr('class', review.date == 0 ? 'alert' : 'alert alert-info')
				;

			$('textarea', box).val(review.rcontent);
			$('.review-save', box).click(saveReview);

			r.append(box);
			r.append('<hr/>');
		};

		bindButtonEvent();
		sh_highlightDocument();

	});
	return false;
}

;function viewFeedback(twizid) {
	$.getJSON('/viewfb/'+twizid, function (d) {
		if(!$.isArray(d)) {
			$.gritter.add({
				title: 'Error',
				class_name: 'alert-error',
				text: 'Error occurred on loading feedbacks.'
			});
			return;
		}
		var converter = new Attacklab.showdown.converter();

		// Fill in fbs to work with
		var r = $('#myfb-list').empty();
		for (var i = 0; i < d.length; i++) {
			var fb = d[i];
			var box = $('#fb-box-source').clone().attr('id', '').removeClass('hide');
			$('span.ruser', box).text(fb.ruser);
			$('div.rcontent', box).html(converter.makeHtml(fb.rcontent ? fb.rcontent : '**No feedback submitted so far.**'));

			r.append(box);
		};
	});
	return false;
}

;function saveSolution()
{
	var form = $(this).parents('form');
	var postdata = form.serialize();
	var twizid = $('#twiz-list .active a').attr('twizid');
	$.post('/posttwiz/'+twizid, postdata, _postCallback, 'json');
}

;function saveReview()
{
	var form = $(this).parents('form');
	var ruser = $('.ruser', form.parent()).text();
	var postdata = form.serialize();
	var twizid = $('#twiz-list .active a').attr('twizid');
	$.post('/postreview/'+twizid+'/'+ruser, postdata, _postCallback, 'json');
}

;function _postCallback(d)
{
	if(d.error == 0) {
		$.gritter.add({
			title: 'Success',
			class_name: 'alert-success',
			text: d.length + ' bytes written.<br />' + d.record + ' records saved.'
		});
		viewTwiz(twizid);
	} else {
		$.gritter.add({
			title: 'Error',
			class_name: 'alert-error',
			sticky: true,
			text: d.reason
		});
	}

}

;function bindButtonEvent()
{
	$('.form-expand').unbind().click(function (){
		var form = $(this).parent().nextAll('form').fadeIn();
		$.smoothScroll({scrollTarget: form, offset: -60});
		return false;
	});
	$('.form-collapse').unbind().click(function (){
		$(this).parents('form').fadeOut();
		return false;
	});

}

;$(function () {
	var user = $.cookie('user');
	var salt = $.cookie('salt');
	if (user && salt) {
		$('#login-user .username').text(user);
		listTwiz();
	}
	else {
		$('#login-user').text('Please login');
		$('#login-ui').show();
		$('#loading-ui').hide();
	}

	$('.solution-save').click(saveSolution);

	bindButtonEvent();

});