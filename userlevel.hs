module UserLevel where

import Happstack.Server
import Web.Routes              (RouteT)
import Web.Routes.TH           (derivePathInfo)
import Database.HDBC
import Database.HDBC.MySQL
import Control.Monad.IO.Class  (liftIO)
import Data.String.Utils       (join)
import Web.Routes.Happstack    (implSite)
--import Database.HDBC.SqlValue
--import Data.Convertible.Base

import Common

-- List all twiz problems

listTwiz :: String -> RouteT Sitemap (ServerPartT IO) String
listTwiz user = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT id, name, sum(sdate>0), sum(rdate>0) \
			\FROM twiz \
			\LEFT JOIN solution \
			\ON twiz.id=solution.twizid AND suser=? \
			\LEFT JOIN review \
			\ON twiz.id=review.twizid AND ruser=? \
			\WHERE due > 0 \
			\GROUP BY twiz.id \
			\ORDER BY due DESC, rdue DESC"
			[toSql user, toSql user]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [rowid, name, scount, rcount] = 
				"{\"id\":" ++(fromSql rowid)
				++ ",\"name\":" ++ (parseString $ fromSql name)
				++ ",\"scount\":" ++ (parseInt $ fromSql scount)
				++ ",\"rcount\":" ++ (parseInt $ fromSql rcount)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	ok $ json


-- Show the details and user's solution of a twiz problem.

viewTwiz :: String -> Int -> RouteT Sitemap (ServerPartT IO) String
viewTwiz user tid = do

	-- fetch the problem
	problem <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT * FROM twiz WHERE id=? LIMIT 1" [toSql tid]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [rowid, name, content, due, rdue] = 
				"{\"id\":" ++(fromSql rowid)
				++ ",\"name\":" ++ (parseString $ fromSql name)
				++ ",\"content\":" ++ (parseString $ fromSql content)
				++ ",\"due\":" ++ (fromSql due)
				++ ",\"rdue\":" ++ (fromSql rdue)
				++ "}"
		disconnect conn
		return (if null records then "null" else head records)

	-- fetch user's solution
	solution <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT sdate, scontent FROM solution WHERE twizid=? AND suser=? LIMIT 1" [toSql tid, toSql user]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [sdate, scontent] = 
				"{\"date\":" ++ (parseString $ fromSql sdate)
				++ ",\"content\":" ++ (parseString $ fromSql scontent)
				++ "}"
		disconnect conn
		return (if null records then "null" else head records)
	ok $ "{\"problem\":" ++ problem ++ ",\"solution\":" ++ solution ++ "}"


-- Load review tasks that assigned to the user

viewReview :: String -> Int -> RouteT Sitemap (ServerPartT IO) String
viewReview user tid = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT count(*) from twiz \
			\WHERE id = ? AND due < UNIX_TIMESTAMP()" [toSql tid]
		case (fromSql :: SqlValue -> Int) $ head $ head r of
			0 -> return ("[]")
			_ -> do
				r <- quickQuery' conn "SELECT rdate, foruser, rcontent, scontent \
					\FROM review \
					\LEFT JOIN solution \
					\ON (foruser = suser AND review.twizid = solution.twizid) \
					\WHERE review.twizid=? AND ruser=? \
					\ORDER BY rdate ASC" [toSql tid, toSql user]
				let records = map convRec r where
					convRec :: [SqlValue] -> String
					convRec [rdate, foruser, rcontent, scontent] = 
						"{\"date\":" ++(fromSql rdate)
						++ ",\"foruser\":" ++ (parseString $ fromSql foruser)
						++ ",\"rcontent\":" ++ (parseString $ fromSql rcontent)
						++ ",\"scontent\":" ++ (parseString $ fromSql scontent)
						++ "}"
				disconnect conn
				return ("[" ++ join "," records ++ "]")
	ok $ json

postTwiz :: String -> Int -> RouteT Sitemap (ServerPartT IO) String
postTwiz user tid = do
	decodeBody (defaultBodyPolicy "/tmp/" 1024 1048576 1024)
	content <- look "content"
	json <- liftIO $ do
		conn <- db
		-- Check time
		r <- quickQuery' conn "SELECT count(*) from twiz \
			\WHERE id = ? AND due >= UNIX_TIMESTAMP()" [toSql tid]
		case (fromSql :: SqlValue -> Int) $ head $ head r of
			0 -> return ("{\"error\":1,\"reason\":\"Past due date\"}")
			_ -> do
				n <- run conn "REPLACE INTO solution \
					\(twizid, suser, sdate, scontent) \
					\VALUES (?, ?, UNIX_TIMESTAMP(), ?)"
					[toSql tid, toSql user, toSql content]
				return ("{\"error\":0,\"length\":" ++ (show $ length content) ++ ",\"record\":" ++ (show $ min 1 n) ++ "}")
	ok $ json

postReview :: String -> Int -> String -> RouteT Sitemap (ServerPartT IO) String
postReview user tid foruser = do
	decodeBody (defaultBodyPolicy "/tmp/" 1024 1048576 1024)
	content <- look "content"
	json <- liftIO $ do
		conn <- db
		-- Check time
		r <- quickQuery' conn "SELECT count(*) from twiz \
			\WHERE id = ? AND rdue >= UNIX_TIMESTAMP()" [toSql tid]
		case (fromSql :: SqlValue -> Int) $ head $ head r of
			0 -> return ("{\"error\":1,\"reason\":\"Past due date\"}")
			_ -> do
				n <- run conn "UPDATE review \
					\SET rcontent = ?, rdate = UNIX_TIMESTAMP() \
					\WHERE twizid = ? AND ruser = ? AND foruser = ?"
					[toSql content, toSql tid, toSql user, toSql foruser]
				return ("{\"error\":0,\"length\":" ++ (show $ length content) ++ ",\"record\":" ++ (show $ min 1 n) ++ "}")
	ok $ json

viewFeedback :: String -> Int -> RouteT Sitemap (ServerPartT IO) String
viewFeedback user tid = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT ruser, rcontent \
			\FROM review \
			\WHERE twizid=? AND foruser=?"
			[toSql tid, toSql user]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [ruser, rcontent] = 
				"{\"ruser\":" ++ (parseString $ fromSql ruser)
				++ ",\"rcontent\":" ++ (parseString $ fromSql rcontent)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	ok $ json

login :: String -> String -> RouteT Sitemap (ServerPartT IO) String
login user passSalt = do
	addCookie Session (mkCookie "user" user)
	addCookie Session (mkCookie "salt" passSalt)
	found "/" "Logged in, proceed"

reg :: String -> String -> RouteT Sitemap (ServerPartT IO) String
reg user passSalt = do
	liftIO $ do
		conn <- db
		run conn "REPLACE INTO user (name, salt) VALUES (?, ?)" [toSql user, toSql passSalt]
		commit conn
		disconnect conn
	ok $ "User registered: " ++ user ++ " " ++ passSalt

