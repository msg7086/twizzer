<?php
$ticket = isset($_GET['ticket']) ? $_GET['ticket'] : '';
$return = isset($_COOKIE['return']) ? $_COOKIE['return'] : '';

if(!$ticket)
{
	header('location: https://login.oregonstate.edu/cas/login?service=' . $_SERVER['SCRIPT_URI']);
	if(empty($return))
		setcookie('return', $_SERVER['HTTP_REFERER']);
	die;
}

$data = file_get_contents('https://login.oregonstate.edu/cas/serviceValidate?ticket=' . $ticket . '&service=' . $_SERVER['SCRIPT_URI']);

$body = trim(strip_tags($data));
if(strpos($data, 'authenticationSuccess'))
{
	$username = $body;
	$usersalt = md5($username . '$' . md5($_SERVER['HTTP_USER_AGENT']));
	if($return)
	{
		setcookie('return', '', 1);
		file_get_contents($return . 'register/' . $username . '/' . $usersalt . '/');
		header('location: ' . $return . 'login/' . $username . '/' . $usersalt . '/');
		exit;
	}
	echo 'Success!';
}
echo $body;

