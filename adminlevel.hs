module AdminLevel where

import Happstack.Server
import Web.Routes              (RouteT)
import Web.Routes.TH           (derivePathInfo)
import Database.HDBC
import Database.HDBC.MySQL
import Control.Monad.IO.Class  (liftIO)
import Data.String.Utils       (join)
import Web.Routes.Happstack    (implSite)
import Data.List.Split
--import Database.HDBC.SqlValue
--import Data.Convertible.Base

import Common

listTwiz::RouteT Sitemap (ServerPartT IO) String
listTwiz = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT id, name, due, rdue \
			\FROM twiz \
			\ORDER BY due DESC, rdue DESC" []
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [rowid, name, due, rdue] = 
				"{\"id\":" ++(fromSql rowid)
				++ ",\"name\":" ++ (parseString $ fromSql name)
				++ ",\"due\":" ++ (parseInt $ fromSql due)
				++ ",\"rdue\":" ++ (parseInt $ fromSql rdue)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	ok $ json

createTwiz:: RouteT Sitemap (ServerPartT IO) String
createTwiz = do
	json <- liftIO $ do
		conn <- db
		n <- run conn "INSERT INTO twiz \
			\(name, content, due, rdue) \
			\VALUES ('Untitled', '', 0, 0)" []
		case n of
			0 -> return ("{\"error\":1,\"reason\":\"Cannot insert record\"}")
			_ -> return ("{\"error\":0}")

	ok $ json

viewTwiz :: Int -> RouteT Sitemap (ServerPartT IO) String
viewTwiz tid = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT * FROM twiz WHERE id=? LIMIT 1" [toSql tid]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [rowid, name, content, due, rdue] = 
				"{\"id\":" ++(fromSql rowid)
				++ ",\"name\":" ++ (parseString $ fromSql name)
				++ ",\"content\":" ++ (parseString $ fromSql content)
				++ ",\"due\":" ++ (fromSql due)
				++ ",\"rdue\":" ++ (fromSql rdue)
				++ "}"
		disconnect conn
		return (if null records then "null" else head records)
	ok $ json

editTwiz::Int -> RouteT Sitemap (ServerPartT IO) String
editTwiz tid = do
	decodeBody (defaultBodyPolicy "/tmp/" 1024 1048576 1024)
	name <- look "name"
	content <- look "content"
	due <- look "due"
	rdue <- look "rdue"
	json <- liftIO $ do
		conn <- db
		n <- run conn "REPLACE INTO twiz \
			\(id, name, content, due, rdue) \
			\VALUES (?, ?, ?, ?, ?)"
			[toSql tid, toSql name, toSql content, toSql due, toSql rdue]
		return ("{\"error\":0,\"length\":" ++ (show $ length content) ++ ",\"record\":" ++ (show $ min 1 n) ++ "}")
	ok $ json

deleteTwiz::Int -> RouteT Sitemap (ServerPartT IO) String
deleteTwiz tid = do
	json <- liftIO $ do
		conn <- db
		n <- run conn "DELETE FROM twiz \
			\WHERE id = ?"
			[toSql tid]
		return ("{\"error\":0,\"record\":" ++ show n ++ "}")
	ok $ json

getAssignBuddy::Int -> RouteT Sitemap (ServerPartT IO) String
getAssignBuddy tid = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT name, enabled, scount, rcount, foruser \
			\FROM user \
			\LEFT JOIN ( \
				\SELECT suser, SUM(sdate > 0) scount \
				\FROM solution \
				\WHERE twizid = ? \
				\GROUP BY suser \
			\) derived_s \
			\ON name = suser \
			\LEFT JOIN ( \
				\SELECT ruser, SUM(rdate > 0) rcount, GROUP_CONCAT(foruser) foruser \
				\FROM review \
				\WHERE twizid = ? \
				\GROUP BY ruser \
			\) derived_r \
			\ON name = ruser \
			\GROUP BY name \
			\ORDER BY enabled DESC, name ASC"
			[toSql tid, toSql tid]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [name, enabled, scount, rcount, foruser] = 
				"{\"name\":" ++ (parseString $ fromSql name)
				++ ",\"enabled\":" ++ (parseInt $ fromSql enabled)
				++ ",\"scount\":" ++ (parseInt $ fromSql scount)
				++ ",\"rcount\":" ++ (parseInt $ fromSql rcount)
				++ ",\"foruser\":" ++ (parseString $ fromSql foruser)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	ok $ json


type ReviewBuddy = (String, String)
-- POSTDATA: luxi:liug,xushua;liug:xushua,luxi;xushua:luxi,liug
-- RETURN: {"error":0,"record":6}
splitAssignData::String -> [ReviewBuddy]
splitAssignData str = concat $ map (splitEntryData) $ splitOn ";" str
		where
			splitEntryData str = splitUsers $ splitOn ":" str
				where
					splitUsers (ruser:forusers:_) = map (assignBuddy ruser) $ splitOn "," forusers
						where assignBuddy ruser foruser = (ruser, foruser)
					splitUsers _ = []

filterSameUser::ReviewBuddy -> Bool
filterSameUser (ruser, foruser) = ruser /= foruser

assignReview::Int -> RouteT Sitemap (ServerPartT IO) String
assignReview tid = do
	decodeBody (defaultBodyPolicy "/tmp/" 1024 1048576 1024)
	postData <- look "buddies"
	json <- liftIO $ do
		conn <- db
		stmt <- prepare conn "INSERT IGNORE INTO review \
			\(twizid, ruser, rdate, foruser, rcontent) \
			\VALUES (?, ?, 0, ?, '')"

		executeMany stmt 
			$ map (\(ruser, foruser) ->
					[toSql tid, toSql ruser, toSql foruser]
				)
			$ filter filterSameUser
			$ splitAssignData postData

		return ("{\"error\":0}")
	ok $ json

listUser::RouteT Sitemap (ServerPartT IO) String
listUser = do
	json <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT name, enabled, scount, rcount \
			\FROM user \
			\LEFT JOIN ( \
				\SELECT suser, SUM(sdate > 0) scount \
				\FROM solution \
				\GROUP BY suser \
			\) derived_s \
			\ON name = suser \
			\LEFT JOIN ( \
				\SELECT ruser, SUM(rdate > 0) rcount \
				\FROM review \
				\GROUP BY ruser \
			\) derived_r \
			\ON name = ruser \
			\GROUP BY name \
			\ORDER BY enabled DESC, name ASC"
			[]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [name, enabled, scount, rcount] = 
				"{\"name\":" ++ (parseString $ fromSql name)
				++ ",\"enabled\":" ++ (parseInt $ fromSql enabled)
				++ ",\"scount\":" ++ (parseInt $ fromSql scount)
				++ ",\"rcount\":" ++ (parseInt $ fromSql rcount)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	ok $ json

switchUser::String -> RouteT Sitemap (ServerPartT IO) String
switchUser user = do
	json <- liftIO $ do
		conn <- db
		n <- run conn "UPDATE user \
			\SET enabled = 1 - enabled \
			\WHERE name = ?"
			[toSql user]
		return ("{\"error\":0,\"record\":" ++ show n ++ "}")
	ok $ json

reportTwiz::Int -> RouteT Sitemap (ServerPartT IO) String
reportTwiz tid = do
	-- fetch user's solution
	solution <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT suser, sdate, scontent \
			\FROM solution \
			\WHERE twizid=?"
			[toSql tid]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [suser, sdate, scontent] = 
				"{\"sdate\":" ++ (parseInt $ fromSql sdate)
				++ ",\"suser\":" ++ (parseString $ fromSql suser)
				++ ",\"scontent\":" ++ (parseString $ fromSql scontent)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")
	-- fetch the review
	review <- liftIO $ do
		conn <- db
		r <- quickQuery' conn "SELECT ruser, rdate, foruser, rcontent \
			\FROM review \
			\WHERE twizid=?"
			[toSql tid]
		let records = map convRec r where
			convRec :: [SqlValue] -> String
			convRec [ruser, rdate, foruser, rcontent] = 
				"{\"ruser\":" ++ (parseString $ fromSql ruser)
				++ ",\"rdate\":" ++ (parseInt $ fromSql rdate)
				++ ",\"foruser\":" ++ (parseString $ fromSql foruser)
				++ ",\"rcontent\":" ++ (parseString $ fromSql rcontent)
				++ "}"
		disconnect conn
		return ("[" ++ join "," records ++ "]")

	ok $ "{\"solution\":" ++ solution ++ ",\"review\":" ++ review ++ "}"

-- TODO: Set Content-Type on Response
saveReport::RouteT Sitemap (ServerPartT IO) String
saveReport =  do
	decodeBody (defaultBodyPolicy "/tmp/" 1024 1048576 1024)
	content <- look "content"
	ok $ "<!DOCTYPE html>\
		\<html lang=\"en\">\
		\  <head>\
		\    <meta charset=\"utf-8\">\
		\    <title>Twizzer report</title>\
		\    <link rel=\"stylesheet\" href=\"/assets/css/bootstrap.css\">\
		\    <link rel=\"stylesheet\" href=\"/assets/css/bootstrap-responsive.css\">\
		\    <link rel=\"stylesheet\" href=\"/assets/css/sh_solarized.css\">\
		\    <link rel=\"stylesheet\" href=\"/assets/css/twiz.css\">\
		\  </head>\
		\  <body><div class=\"container export\">"
		++ content
		++ "</div></body></html>"